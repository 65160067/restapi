import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsius') celcius: string) {
    return this.temperatureService.convert(parseFloat(celcius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celcius: number) {
    return this.temperatureService.convert(celcius);
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celcius: string) {
    return this.temperatureService.convert(parseFloat(celcius));
  }
}
